package Bean;

import Utils.Constants;

public class GrosOutil implements Outil {
	private String nom;

	public GrosOutil(String nom) {
		this.nom = nom;
	}

	@Override
	public int getCodePrix() {
		return Constants.GROS_OUTILLAGE;
	}

	public String getTitre(){
		return nom;
	}
	public double getDueForThisItem(int nbDays){
		double due = 1.5;
		if (nbDays > 3){
			due += (nbDays - 3) * 1.5;
		}
		return due;
	}
}
