package Bean;

import Utils.*;

public class NouvelOutil implements Outil {
	private String nom;

	public NouvelOutil(String nom) {
		this.nom = nom;
	}

	@Override
	public int getCodePrix() {
		return Constants.NOUVEAUTEES;
	}
	
	public String getTitre(){
		return nom;
	}
	
	public double getDueForThisItem(int nbDays){
		return nbDays * 3;
	}
}
