package Bean;

public interface Outil {
	public int getCodePrix();
	public String getTitre();
	public double getDueForThisItem(int nbDays);
}
