package Bean;

import Utils.Constants;

public class RegulierOutil implements Outil {
	private String nom;

	public RegulierOutil(String nom) {
		this.nom = nom;
	}

	@Override
	public int getCodePrix() {
		return Constants.REGULIER;
	}

	public String getTitre(){
		return nom;
	}
	
	public double getDueForThisItem(int nbDays){
		double due = 2;
		if (nbDays > 2){
			due += (nbDays - 2) * 1.5;
		}
		return due;
	}
}
