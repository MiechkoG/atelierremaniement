package ca.qc.claurendeau;

import java.util.Enumeration;
import java.util.Vector;

import Bean.NouvelOutil;
import Bean.Outil;
import Bean.RegulierOutil;
import Utils.Constants;

/**
 * La classe Client permet d’identifier les clients du magasin
 * 
 * @author 03738
 * 
 */
public class Client {
	private String clientName;
	private Vector<Location> listLocation = new Vector<Location>();
	private int bonusPoints = 0;
	private double amountDue = 0;

	public Client(String nom) {
		clientName = nom;
	}

	public String getNom() {
		return clientName;
	}

	public int getPointsBoni() {
		return bonusPoints;
	}

	public double getMontantDu() {
		return amountDue;
	}

	public void ajoutLocation(Location arg) {
		listLocation.addElement(arg);
		bonusPoints++;
		if (arg.getOutil().getCodePrix() == Constants.NOUVEAUTEES && arg.getNbJoursLoues() > 1) {
			bonusPoints++;
		}
	}

	public String rapport() {
		Enumeration<Location> locations = listLocation.elements();

		String result = "Etat des locations effectuées de " + getNom() + " :\n";

		result = calculateMontantDu(locations, result);

		result += "Montant du égale à : " + amountDue + "\n";
		result += "Vous gagnez : " + bonusPoints + " points de bonis";

		return result;
	}

	public String rapportHTML() {
		Enumeration<Location> locations = listLocation.elements();

		String result = "<div><h1>Etat des locations effectuées de " + getNom() + " :</h1>\n";

		result = "<h4>" + calculateMontantDu(locations, result) + "</h4>";

		result += "<h4>Montant du égale à : " + amountDue + "</h4>\n";
		result += "<h4>Vous gagnez : " + bonusPoints + " points de bonis</h4></div>";

		return result;
	}

	private String calculateMontantDu(Enumeration<Location> locations, String result) {
		while (locations.hasMoreElements()) {
			Location currentLocation = (Location) locations.nextElement();
			Outil currentOutil = currentLocation.getOutil();
			double dueForThisItem = currentOutil.getDueForThisItem(currentLocation.getNbJoursLoues());
			result += "\t" + currentOutil.getTitre() + "\t" + dueForThisItem + "\n";
			amountDue += dueForThisItem;
		}
		return result;
	}

	public static void main(String[] args) {
		Client c = new Client("Sallie");
		Outil outil1 = new NouvelOutil("Marteau piqueur");
		Location locat1 = new Location(outil1, 3); // 3 jours de location
		c.ajoutLocation(locat1);
		System.out.println(c.rapportHTML());
	}
}
